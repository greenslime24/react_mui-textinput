import React, { useState }  from "react";
import { TextField } from "@mui/material";

export default function Search(props){
    
    return(
        <div>
            <TextField
            label="search"
            variant="filled"
            onChange={(e)=>{
                props.getsrc(e.target.value)
            }}/>
        </div>
    )
}

